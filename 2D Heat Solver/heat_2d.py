import numpy as np
import matpy as mp
import scipy as sp
import scipy.sparse as spr
import matplotlib.pyplot as plt
import sys

# Start timer
import timing

# Temporal parameters
t0 = 0.
tf = 200
tplot = 0.5

# x-grid parameters
minx = -1.
maxx =  1.
Nx   = 100

# y-grid paramters
miny = -1
maxy =  1.
Ny   = 100

# diffusion parameter
gamma = 0.1

# Initial conditions, source functions, diffusion function
IC  = lambda x,y : 0*np.exp(- (x/0.2)**2 - (y/0.2)**2)
SRC = lambda x,y : 5*(np.exp(- ((x-0.5)/0.1)**2 - (y/0.1)**2) - np.exp(- ((x+0.5)/0.1)**2 - (y/0.1)**2))
gamma = lambda x,y : 0.1*(0.01 + (X**2 + Y**2 > 0.1))

# Create the grid
x = np.linspace(minx,maxx,Nx)
y = np.linspace(miny,maxy,Ny)

# Determine the time step
dx = x[1] - x[0]
dy = y[1] - y[0]
dt = 0.45*min(dx**2,dy**2)

# Write the grid to a file for processing
np.savez('Outputs/0_grid.npz', my_x=x, my_y=y, \
        my_nx=Nx, my_ny=Ny, full_x=x, full_y=y, \
        Nx=Nx, Ny=Ny, nprc=1, tplot=tplot)

# Create differentiation arrays
Dx2 = mp.FiniteDiff(x, 2, DiffOrd=2, Periodic=True, spb=True)
Ix  = spr.eye(Nx)
Dy2 = mp.FiniteDiff(y, 2, DiffOrd=2, Periodic=True, spb=True)
Iy  = spr.eye(Ny)

DX2 = spr.kron(Dx2,Iy)
DY2 = spr.kron(Ix,Dy2)
LAP = DX2 + DY2

# Make the full grid
x,y = np.meshgrid(x,y,indexing='ij')
X = x.ravel()
Y = y.ravel()

# Evaluate initial conditions, sources, and diffusion functions
q = IC(X,Y)
S = SRC(X,Y)
G = gamma(X,Y)

t = t0
next_plot = tplot
cnt = 1

# Print time
timing.log('Starting loop')

# Loop until we hit the final time
while t < tf:

    # Compute the flux
    flux = (G*LAP.dot(q) + S)*dt
    q += flux
    
    # Output, if necessary
    if t + dt >= next_plot:
        np.save('Outputs/0_{0:d}.npy'.format(cnt), q.reshape((Nx,Ny)))
        next_plot += tplot
        cnt += 1

    # Increment time
    t += dt

# Print time
timing.log('Done.')
