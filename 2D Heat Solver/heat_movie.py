import numpy as np
import matplotlib.pyplot as plt
import glob
import subprocess

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprc = comm.Get_size()

# Load the grid information and get some useful numbers
gp = np.load('Outputs/0_grid.npz')
Nx = gp['Nx']
Ny = gp['Ny']
Np = gp['nprc']
tplot = gp['tplot']

# Get a list of all of the output files
files = glob.glob("Outputs/0_*.npy")
nfiles = len(files)

# Loop through each file
for ii in range(rank,nfiles,nprc):

    curr_file = files[ii]

    out_num = int(curr_file[10:-4])

    to_plot = np.zeros((Nx,Ny))
    x_cnt = 0

    # If we ran the MPI version, we need to stitch together 
    # the output from each processor. Do that now.
    for jj in range(Np):

        fl = "Outputs/{0:d}_{1:d}.npy".format(jj,out_num)
        data = np.load(fl)
        local_nx = data.shape[0] - 2

        if data.shape == (Nx,Ny):
            to_plot = data
        else:
            to_plot[x_cnt:x_cnt+local_nx,:] = data[1:-1,1:-1]
        x_cnt += local_nx

    # Plot the output.
    plt.figure()
    plt.pcolormesh(gp['full_x'],gp['full_y'],to_plot.T,cmap='plasma')
    plt.colorbar()
    plt.title('t = {0:.4g}'.format(tplot*out_num))
    plt.savefig('Frames/{0:04d}.png'.format(out_num))
    plt.close()

# Now stitch the frames into a movie
make_movie_cmd = ['ffmpeg', '-framerate', '10', '-r', '10', \
       '-i', 'Frames/%04d.png', '-y', '-q', '1', '-pix_fmt', 'yuv420p', 'movie.mp4']
subprocess.call(make_movie_cmd)        

