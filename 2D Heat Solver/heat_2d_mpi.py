
## This code solves the 2D heat equation
## On a rectangular and periodic domain.
## Arbitrary initial conditions, source terms,
## and thermal diffusivity can be applied.

import numpy as np
import matpy as mp
import scipy as sp
import scipy.sparse as spr
import matplotlib.pyplot as plt
import sys

# Start timer
import timing

# Initialize MPI
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprc = comm.Get_size()

# Temporal parameters
t0 = 0.
tf = 20.
tplot = 0.1

# x-grid parameters
minx = -1.
maxx =  1.
Nx   = 100

# y-grid paramters
miny = -1
maxy =  1.
Ny   = 100

# diffusion parameter
gamma = 0.1

# Initial conditions, source functions, diffusion function
IC  = lambda x,y : 0*np.exp(- (x/0.2)**2 - (y/0.2)**2)
SRC = lambda x,y : 5*(np.exp(- ((x-0.5)/0.1)**2 - (y/0.1)**2) - np.exp(- ((x+0.5)/0.1)**2 - (y/0.1)**2))
gamma = lambda x,y : 0.1*(0.01 + (X**2 + Y**2 > 0.1))

# Create the grid
full_x = np.linspace(minx, maxx, Nx)
full_y = np.linspace(miny, maxy, Ny)

# Determine the time step
dx = full_x[1] - full_x[0]
dy = full_y[1] - full_y[0]
dt = 0.45*min(dx**2,dy**2)

# Now let's figure out the local stuff
# For the moment, only split in x, because I'm lazy
Nx_pp = int(Nx/nprc)
if rank < nprc - 1:
    my_nx = Nx_pp
else:
    my_nx = Nx - Nx_pp*(nprc-1)

my_ny = Ny

my_x = np.zeros(my_nx + 2)
my_y = np.zeros(my_ny + 2)

my_x[1:-1] = full_x[Nx_pp*rank : Nx_pp*rank+my_nx]
my_x[0]    = my_x[1]  - dx # doesn't actually matter
my_x[-1]   = my_x[-2] + dx # doesn't actually matter

my_y[1:-1] = full_y
my_y[0]    = my_y[1]  - dy # again, doesn't matter
my_y[-1]   = my_y[-2] + dy # again, doesn't matter

# Write the grid to a file for processing
np.savez('Outputs/{0:d}_grid.npz'.format(rank), my_x=my_x, my_y=my_y, \
        my_nx=my_nx, my_ny=my_ny, full_x=full_x, full_y=full_y, \
        Nx=Nx, Ny=Ny, nprc=nprc, tplot=tplot)

# Create differentiation arrays
Dx2 = mp.FiniteDiff(my_x, 2, DiffOrd=2, spb=True, uniform=True)
Ix  = spr.eye(my_nx+2)
Dy2 = mp.FiniteDiff(my_y, 2, DiffOrd=2, spb=True, uniform=True)
Iy  = spr.eye(my_ny+2)

DX2 = spr.kron(Dx2,Iy)
DY2 = spr.kron(Ix,Dy2)

LAP = DX2 + DY2

# Now create our local grid and initial state
X,Y = np.meshgrid(my_x,my_y,indexing='ij')

q = IC(X,Y)
S = SRC(X,Y)
G = gamma(X,Y)

# The meat of the parallel approach: sharing ghost cells
left  = (rank - 1)%nprc
right = (rank + 1)%nprc

tmp_left = np.zeros(Ny+2)
tmp_right = np.zeros(Ny+2)

to_send = q[-2,:]
comm.Sendrecv(to_send, dest = right, recvbuf = tmp_left, source = left)

to_send = q[1,:]
comm.Sendrecv(to_send, dest = left, recvbuf = tmp_right, source = right)

q[0,:]  = tmp_left
q[-1,:] = tmp_right

q[:,-1] = q[:,1]
q[:,0]  = q[:,-2]

t = t0
next_plot = tplot
cnt = 1

# Print time
if rank == 0:
    timing.log("Starting loop")

# Loop until we hit the final time
while t < tf:

    # Compute the flux on our part of the domain
    flux = (G.ravel()*LAP.dot(q.ravel()) + S.ravel())*dt
    q += flux.reshape(my_nx+2,my_ny+2)

    # Share the new ghost cells
    to_send = q[-2,:]
    comm.Sendrecv(to_send, dest = right, recvbuf = tmp_left, source = left)

    to_send = q[1,:]
    comm.Sendrecv(to_send, dest = left, recvbuf = tmp_right, source = right)

    q[0,:]  = tmp_left
    q[-1,:] = tmp_right
    q[:,-1] = q[:,1]
    q[:,0]  = q[:,-2]

    # Output, if necessary
    if t + dt >= next_plot:
        np.save('Outputs/{0:d}_{1:d}.npy'.format(rank,cnt), q)
        next_plot += tplot
        cnt += 1
        
    t += dt

# Print time
if rank == 0:
    timing.log("Done.")
