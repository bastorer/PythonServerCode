# Depending on our research environments, we may often find ourselves
# working on remote servers that do not include display options
# (e.g. submitted jobs on SharcNet do no have display access).
# In order to be able to safely render images, we need to change the
# default renderer.
import matplotlib
matplotlib.use('Agg')
# Now we can plot as usual.

import matplotlib.pyplot as plt
import numpy as np

# Let's start by creating the same plot as we did in the notebook.
# Notice that plot creation is identical when using non-notebook python. 

x = np.linspace(0,4*np.pi,100)
sinx = np.sin(x)
approxs = [x]
for ii in range(1,10):
    approxs += [approxs[ii-1] + (-1.)**ii/np.math.factorial(2*ii+1)*x**(2*ii+1) ]

plt.figure()                                      # Create a new figure
plt.plot(x, sinx, label='$sin(x)$', linewidth=2)  # Plot the sine curve
for ii in range(0,10,2):
    plt.plot(x, approxs[ii], label='ord. {0:d}'.format(1+ii), linewidth=2)
plt.legend(loc='best')                            # Add a legend (uses the labels given above) 
plt.axis('tight')                                 # Tighten the axes to fit perfectly
plt.title('Polynomial Approximations to $sin(x)$')# Add a title
plt.xlabel('x')                                   # Add x label
plt.ylabel('y')                                   # Add y label
plt.ylim(-2,2)                                    # Manually tighten vertical axis


# In this case, we don't want to call plt.show() since there is no display
# Instead, we will directly save the figure to a file.
print('This script does not display anything. Instead, it saved several image files.')
print('This type of plotting is useful for large scripts and plotting on remote servers.')
plt.savefig('Sine_approx.png')
plt.savefig('Sine_approx.pdf')
plt.savefig('Sine_approx.eps')
