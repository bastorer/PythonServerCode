import matplotlib.pyplot as plt
import numpy as np

# Let's start by creating the same plot as we did in the notebook.
# Notice that plot creation is identical when using non-notebook python. 

x = np.linspace(0,4*np.pi,100)
sinx = np.sin(x)
ord1 = x
ord2 = ord1 - (1./np.math.factorial(3))*x**3
ord3 = ord2 + (1./np.math.factorial(5))*x**5
ord4 = ord3 - (1./np.math.factorial(7))*x**7

plt.figure()                                      # Create a new figure
plt.plot(x, sinx, label='$sin(x)$', linewidth=2)  # Plot the sine curve
plt.plot(x, ord1, label='ord1', linewidth=2)      # Plot first approx
plt.plot(x, ord2, label='ord2', linewidth=2)      # Plot second approx
plt.plot(x, ord3, label='ord3', linewidth=2)      # Plot third approx
plt.plot(x, ord4, label='ord4', linewidth=2)      # Plot fourth approx
plt.legend(loc='best')                            # Add a legend (uses the labels given above) 
plt.axis('tight')                                 # Tighten the axes to fit perfectly
plt.title('Polynomial Approximations to $sin(x)$')# Add a title
plt.xlabel('x')                                   # Add x label
plt.ylabel('y')                                   # Add y label
plt.ylim(-2,2)                                    # Manually tighten vertical axis

# The main difference is that now we need to explicitely show the figure
# The show command will display all figures that have been generated
plt.show()

# Command-line figures are blocking, that means that while figures are being displayed,
# you cannot interact with the python session.
print('This will not be printed until AFTER the figure has been closed.')


