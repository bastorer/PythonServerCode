
## This code solves the 2D shallow water equations
## nn a rectangular and periodic domain.
## Arbitrary initial conditions

import numpy as np
import matpy as mp
import scipy as sp
import scipy.sparse as spr
import matplotlib.pyplot as plt
import sys

# Start timer
import timing

import pyfftw
from pyfftw.interfaces.scipy_fftpack import fft, ifft

# Number of threads
num_threads = 4

# Temporal parameters
t0 = 0.
tf = 1.
tplot = 0.01

# x-grid parameters
minx = -1.
maxx =  1.
Nx   = 128
Lx   = maxx - minx
dx = Lx/Nx

# y-grid parameters
miny = -1
maxy =  1.
Ny   = 128
Ly   = maxy - miny
dy = Ly/Ny

# Physical parameters
H = 1.
g = 9.81

# Initial conditions
ICu = lambda x,y : 0.*x
ICv = lambda x,y : 0.*x
ICh = lambda x,y : H + 0.1*np.exp(- (x/0.2)**2 - (y/0.2)**2)

x = np.arange(minx+dx/2,maxx,dx)
y = np.arange(miny+dy/2,maxy,dy)

np.savez('Outputs/SW_grid.npz', x=x,y=y,Nx=Nx,Ny=Ny)

# Now create our local grid and initial state
X,Y = np.meshgrid(x,y,indexing='ij')

u = ICu(X,Y)
v = ICv(X,Y)
h = ICh(X,Y)

t = t0
next_plot = tplot
cnt = 1


# Initialize the fftw transforms, wisdowm preserving
Nkx = Nx # Assume periodic
kx = 2*np.pi/Lx*np.hstack([range(0,int(Nx/2)), range(-int(Nx/2),0)])
ik = 1j*np.tile(kx.reshape((Nkx,1)),(1,Ny))

Nky = Ny # Assume periodic
ky = 2*np.pi/Ly*np.hstack([range(0,int(Ny/2)), range(-int(Ny/2),0)])
il = 1j*np.tile(kx.reshape((1,Nky)),(Nx,1))

# x derivatives
tmpx_in_u  = pyfftw.n_byte_align_empty((Nx,Ny),16,dtype='complex128')
tmpx_out_u = pyfftw.n_byte_align_empty((Nkx,Ny),16,dtype='complex128')
fftx_u  = pyfftw.FFTW(tmpx_in_u, tmpx_out_u, axes=[0], direction='FFTW_FORWARD', threads = num_threads)
ifftx_u = pyfftw.FFTW(tmpx_out_u, tmpx_in_u, axes=[0], direction='FFTW_BACKWARD', threads = num_threads)

tmpx_in_v  = pyfftw.n_byte_align_empty((Nx,Ny),16,dtype='complex128')
tmpx_out_v = pyfftw.n_byte_align_empty((Nkx,Ny),16,dtype='complex128')
fftx_v  = pyfftw.FFTW(tmpx_in_v, tmpx_out_v, axes=[0], direction='FFTW_FORWARD', threads = num_threads)
ifftx_v = pyfftw.FFTW(tmpx_out_v, tmpx_in_v, axes=[0], direction='FFTW_BACKWARD', threads = num_threads)

tmpx_in_h  = pyfftw.n_byte_align_empty((Nx,Ny),16,dtype='complex128')
tmpx_out_h = pyfftw.n_byte_align_empty((Nkx,Ny),16,dtype='complex128')
fftx_h  = pyfftw.FFTW(tmpx_in_h, tmpx_out_h, axes=[0], direction='FFTW_FORWARD', threads = num_threads)
ifftx_h = pyfftw.FFTW(tmpx_out_h, tmpx_in_h, axes=[0], direction='FFTW_BACKWARD', threads = num_threads)

def ddx_u(f):
    df = np.real(ifftx_u(ik*fftx_u(f)))
    return df
    
def ddx_v(f):
    df = np.real(ifftx_v(ik*fftx_v(f)))
    return df
    
def ddx_h(f):
    df = np.real(ifftx_h(ik*fftx_h(f)))
    return df

# y derivatives
tmpy_in_u  = pyfftw.n_byte_align_empty((Nx,Ny),16,dtype='complex128')
tmpy_out_u = pyfftw.n_byte_align_empty((Nx,Nky),16,dtype='complex128')
ffty_u  = pyfftw.FFTW(tmpy_in_u, tmpy_out_u, axes=[1], direction='FFTW_FORWARD', threads = num_threads)
iffty_u = pyfftw.FFTW(tmpy_out_u, tmpy_in_u, axes=[1], direction='FFTW_BACKWARD', threads = num_threads)

tmpy_in_v  = pyfftw.n_byte_align_empty((Nx,Ny),16,dtype='complex128')
tmpy_out_v = pyfftw.n_byte_align_empty((Nx,Nky),16,dtype='complex128')
ffty_v  = pyfftw.FFTW(tmpy_in_v, tmpy_out_v, axes=[1], direction='FFTW_FORWARD', threads = num_threads)
iffty_v = pyfftw.FFTW(tmpy_out_v, tmpy_in_v, axes=[1], direction='FFTW_BACKWARD', threads = num_threads)

tmpy_in_h  = pyfftw.n_byte_align_empty((Nx,Ny),16,dtype='complex128')
tmpy_out_h = pyfftw.n_byte_align_empty((Nx,Nky),16,dtype='complex128')
ffty_h  = pyfftw.FFTW(tmpy_in_h, tmpy_out_h, axes=[1], direction='FFTW_FORWARD', threads = num_threads)
iffty_h = pyfftw.FFTW(tmpy_out_h, tmpy_in_h, axes=[1], direction='FFTW_BACKWARD', threads = num_threads)

def ddy_u(f):
    df = np.real(iffty_u(il*ffty_u(f)))
    return df
    
def ddy_v(f):
    df = np.real(iffty_v(il*ffty_v(f)))
    return df
    
def ddy_h(f):
    df = np.real(iffty_h(il*ffty_h(f)))
    return df

# Define the spectral filter
fcut = 0.5
ford = 2.0
fstr = 20.0
k = kx/max(kx.ravel())
filtx = np.exp(-fstr*((np.abs(k)-fcut)/(1-fcut))**ford)*(np.abs(k)>fcut) + (np.abs(k)<fcut)
filtx = filtx.reshape((Nkx,1))
        
k = ky/max(ky.ravel())
filty = np.exp(-fstr*((np.abs(k)-fcut)/(1-fcut))**ford)*(np.abs(k)>fcut) + (np.abs(k)<fcut)
filty = filty.reshape((1,Nky))
        
filt = np.tile(filtx,(1,Nky))*np.tile(filty,(Nkx,1))

# Transforms for the spectral filter
tmpn_in_u  = pyfftw.n_byte_align_empty((Nx,Ny),16,dtype='complex128')
tmpn_out_u = pyfftw.n_byte_align_empty((Nkx,Nky),16,dtype='complex128')
fftn_u  = pyfftw.FFTW(tmpn_in_u, tmpn_out_u, axes=[0,1], direction='FFTW_FORWARD', threads = num_threads)
ifftn_u = pyfftw.FFTW(tmpn_out_u, tmpn_in_u, axes=[0,1], direction='FFTW_BACKWARD', threads = num_threads)

tmpn_in_v  = pyfftw.n_byte_align_empty((Nx,Ny),16,dtype='complex128')
tmpn_out_v = pyfftw.n_byte_align_empty((Nkx,Nky),16,dtype='complex128')
fftn_v  = pyfftw.FFTW(tmpn_in_v, tmpn_out_v, axes=[0,1], direction='FFTW_FORWARD', threads = num_threads)
ifftn_v = pyfftw.FFTW(tmpn_out_v, tmpn_in_v, axes=[0,1], direction='FFTW_BACKWARD', threads = num_threads)

tmpn_in_h  = pyfftw.n_byte_align_empty((Nx,Ny),16,dtype='complex128')
tmpn_out_h = pyfftw.n_byte_align_empty((Nkx,Nky),16,dtype='complex128')
fftn_h  = pyfftw.FFTW(tmpn_in_h, tmpn_out_h, axes=[0,1], direction='FFTW_FORWARD', threads = num_threads)
ifftn_h = pyfftw.FFTW(tmpn_out_h, tmpn_in_h, axes=[0,1], direction='FFTW_BACKWARD', threads = num_threads)

# Filter functions
def filt_u(f):
    df = np.real(ifftn_u(filt*fftn_u(f)))
    return df
    
def filt_v(f):
    df = np.real(ifftn_v(filt*fftn_v(f)))
    return df
    
def filt_h(f):
    df = np.real(ifftn_h(filt*fftn_h(f)))
    return df

# Print time
timing.log("Starting loop")


while t < tf:

    mu = max(np.max(np.abs(u)), np.max(np.abs(v)))
    dt = 0.1*min(dx,dy)/(mu + 2*np.sqrt(g*H))

    # Compute the flux

    # u_t = - u*u_x - v*u_y - g*h_x
    # v_t = - u*v_x - v*v_y - g*h_y
    # u_t = - (hu)_x - (hv)_y
    flux_u = - u*ddx_u(u) - v*ddy_u(u) - g*ddx_h(h)
    flux_v = - u*ddx_v(v) - v*ddy_v(v) - g*ddy_h(h)
    flux_h = - ddx_u(h*u) - ddy_v(h*v)

    # Step forward
    u += dt*flux_u
    v += dt*flux_v
    h += dt*flux_h

    # Filter the new system
    u = filt_u(u)
    v = filt_v(v)
    h = filt_h(h)

    if t + dt >= next_plot:
        np.savez('Outputs/SW_0_{0:d}.npz'.format(cnt), u=u, v=v, h=h, t=t+dt)
        cnt += 1
        next_plot += tplot
        
    t += dt

# Print time
timing.log('Done.')
