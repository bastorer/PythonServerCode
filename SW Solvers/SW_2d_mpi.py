
## This code solves the 2D shallow water equation
## on a rectangular and periodic domain.
## Arbitrary initial conditions
import numpy as np
import matpy as mp
import scipy as sp
import scipy.sparse as spr
import matplotlib.pyplot as plt
import sys

# Start timer
import timing

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprc = comm.Get_size()

# Temporal paramters
t0 = 0.
tf = 1.
tplot = 0.01

# x-grid parameters
minx = -1.
maxx =  1.
Nx   = 200
Lx   = maxx - minx
dx = Lx/Nx

# y-grid parameters
miny = -1
maxy =  1.
Ny   = 200
Ly   = maxy - miny
dy = Ly/Ny

# Physical parameters
H = 1.
g = 9.81

# Initial conditions
ICu = lambda x,y : 0.*x
ICv = lambda x,y : 0.*x
ICh = lambda x,y : H + 0.1*np.exp(- (x/0.2)**2 - (y/0.2)**2)

full_x = np.arange(minx+dx/2,maxx,dx)
full_y = np.arange(miny+dy/2,maxy,dy)

# Now let's figure out the local stuff
# For the moment, only split in x, because I'm lazy
Nx_pp = int(Nx/nprc)
if rank < nprc - 1:
    my_nx = Nx_pp
else:
    my_nx = Nx - Nx_pp*(nprc-1)

my_ny = Ny

my_x = np.zeros(my_nx + 2)
my_y = np.zeros(my_ny + 2)

my_x[1:-1] = full_x[Nx_pp*rank : Nx_pp*rank+my_nx]
my_x[0]    = my_x[1]  - dx 
my_x[-1]   = my_x[-2] + dx

my_y[1:-1] = full_y
my_y[0]    = my_y[1]  - dy
my_y[-1]   = my_y[-2] + dy

# Write the grid to a file for processing
if rank == 0:
    np.savez('Outputs/SW_grid.npz', my_x=my_x, my_y=my_y, \
            my_nx=my_nx, my_ny=my_ny, full_x=full_x, full_y=full_y, \
            Nx=Nx, Ny=Ny, nprc=nprc)

# Create differentiation arrays
Dx = mp.FiniteDiff(my_x, 2, DiffOrd=1, spb=True, uniform=True)
Ix = spr.eye(my_nx+2)
Dy = mp.FiniteDiff(my_y, 2, DiffOrd=1, spb=True, uniform=True)
Iy = spr.eye(my_ny+2)

DX = spr.kron(Dx,Iy)
DY = spr.kron(Ix,Dy)

# Now create our local grid and initial state
X,Y = np.meshgrid(my_x,my_y,indexing='ij')

u = ICu(X,Y)
v = ICv(X,Y)
h = ICh(X,Y)

# The meat of the parallel approach: sharing ghost cells
left  = (rank - 1)%nprc
right = (rank + 1)%nprc

tmp_left = np.zeros(Ny+2)
tmp_right = np.zeros(Ny+2)

to_send = u[-2,:]
comm.Sendrecv(to_send, dest = right, recvbuf = tmp_left, source = left)
to_send = u[1,:]
comm.Sendrecv(to_send, dest = left, recvbuf = tmp_right, source = right)
u[0,:]  = tmp_left
u[-1,:] = tmp_right

to_send = v[-2,:]
comm.Sendrecv(to_send, dest = right, recvbuf = tmp_left, source = left)
to_send = v[1,:]
comm.Sendrecv(to_send, dest = left, recvbuf = tmp_right, source = right)
v[0,:]  = tmp_left
v[-1,:] = tmp_right

to_send = h[-2,:]
comm.Sendrecv(to_send, dest = right, recvbuf = tmp_left, source = left)
to_send = h[1,:]
comm.Sendrecv(to_send, dest = left, recvbuf = tmp_right, source = right)
h[0,:]  = tmp_left
h[-1,:] = tmp_right

u[:,-1] = u[:,1]
u[:,0]  = u[:,-2]
v[:,-1] = v[:,1]
v[:,0]  = v[:,-2]
h[:,-1] = h[:,1]
h[:,0]  = h[:,-2]

t = t0
next_plot = tplot
cnt = 1

# Print time
if rank == 0:
    timing.log("Starting loop")

while t < tf:

    mu = max(np.max(np.abs(u)), np.max(np.abs(v)))
    dt = 0.1*min(dx,dy)/(mu + 2*np.sqrt(g*H))

    # Compute the flux on our part of the domain

    # u_t = - u*u_x - v*u_y - g*h_x
    # v_t = - u*v_x - v*v_y - g*h_y
    # u_t = - (hu)_x - (hv)_y
    flux_u = - u.ravel()*DX.dot(u.ravel()) - v.ravel()*DY.dot(u.ravel()) - g*DX.dot(h.ravel())
    flux_v = - u.ravel()*DX.dot(v.ravel()) - v.ravel()*DY.dot(v.ravel()) - g*DY.dot(h.ravel())
    flux_h = - DX.dot((h*u).ravel()) - DY.dot((h*v).ravel())

    u += dt*flux_u.reshape(my_nx+2, my_ny+2)
    v += dt*flux_v.reshape(my_nx+2, my_ny+2)
    h += dt*flux_h.reshape(my_nx+2, my_ny+2)

    # Share the new ghost cells
    to_send = u[-2,:]
    comm.Sendrecv(to_send, dest = right, recvbuf = tmp_left, source = left)
    to_send = u[1,:]
    comm.Sendrecv(to_send, dest = left, recvbuf = tmp_right, source = right)
    u[0,:]  = tmp_left
    u[-1,:] = tmp_right

    to_send = v[-2,:]
    comm.Sendrecv(to_send, dest = right, recvbuf = tmp_left, source = left)
    to_send = v[1,:]
    comm.Sendrecv(to_send, dest = left, recvbuf = tmp_right, source = right)
    v[0,:]  = tmp_left
    v[-1,:] = tmp_right

    to_send = h[-2,:]
    comm.Sendrecv(to_send, dest = right, recvbuf = tmp_left, source = left)
    to_send = h[1,:]
    comm.Sendrecv(to_send, dest = left, recvbuf = tmp_right, source = right)
    h[0,:]  = tmp_left
    h[-1,:] = tmp_right

    u[:,-1] = u[:,1]
    u[:,0]  = u[:,-2]
    v[:,-1] = v[:,1]
    v[:,0]  = v[:,-2]
    h[:,-1] = h[:,1]
    h[:,0]  = h[:,-2]

    if t + dt >= next_plot:
        np.savez('Outputs/SW_{0:d}_{1:d}.npz'.format(rank,cnt), u=u, v=v, h=h, t=t+dt)
        cnt += 1
        next_plot += tplot
        
    t += dt

# Print time
if rank == 0:
    timing.log("Done.")
