import numpy as np
import matplotlib.pyplot as plt
import glob, subprocess

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprc = comm.Get_size()

gp = np.load('Outputs/SW_grid.npz')
Nx = gp['Nx']
Ny = gp['Ny']

files = glob.glob("Outputs/SW_0_*.npz")
nfiles = len(files)

for ii in range(rank,nfiles,nprc):

    curr_file = files[ii]

    out_num = int(curr_file[13:-4])

    fl = "Outputs/SW_0_{0:d}.npz".format(out_num)
    data = np.load(fl)
    local_nx = data['h'].shape[0] - 2

    u_plot = data['u']
    v_plot = data['v']
    h_plot = data['h']

    plt.figure(figsize=(9,3))

    plt.subplot(1,3,1)
    cv = np.max(np.abs(u_plot))
    plt.pcolormesh(gp['x'],gp['y'],u_plot.T, vmin=-cv, vmax=cv, cmap='seismic')
    plt.axis('equal')
    #plt.colorbar()

    plt.subplot(1,3,2)
    cv = np.max(np.abs(v_plot))
    plt.pcolormesh(gp['x'],gp['y'],v_plot.T, vmin=-cv, vmax=cv, cmap='seismic')
    plt.axis('equal')
    plt.title("t = {0:.4g}".format(float(data['t'])))
    #plt.colorbar()
    
    plt.subplot(1,3,3)
    plt.pcolormesh(gp['x'],gp['y'],h_plot.T,cmap='plasma')
    plt.axis('equal')
    #plt.colorbar()

    plt.tight_layout(True)
    plt.savefig('Frames/SW_{0:04d}.png'.format(out_num))
    plt.close()

        
# Now stitch the frames into a movie
make_movie_cmd = ['ffmpeg', '-framerate', '10', '-r', '10', \
       '-i', 'Frames/SW_%04d.png', '-y', '-q', '1', '-pix_fmt', 'yuv420p', 'movie.mp4']
subprocess.call(make_movie_cmd) 

